
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/*
              0   1  2  3   4
iter1.next => -2, 0, 1, 5, 45, 345
iter2.next => -1, 2, 4, 67, 105
...
superIter.next => -1, 1, 2, 4, 5...
*/

//****************** Прошу не обращайте внимание на дублирование анонимных классов - торопился)))


interface Iterator{
  boolean hasNext();
  int next();
}


class SuperIterator implements Iterator {
  Collection<Iterator> iters;
  private LinkedList<Integer> buffer;
  
  SuperIterator(Collection<Iterator> iters){
    this.iters = iters;
    buffer = new LinkedList<>();
  }
  @Override
  public boolean hasNext(){

    iters.parallelStream().filter(a->a.hasNext()).forEach(a->buffer.add(a.next()));
    Collections.sort(buffer, (a,b)->a-b);
    
    return buffer.size() != 0;
  }
  
  @Override
  public int next(){
      return buffer.pop();
  }
}

//---------------------------------------------------------------------
public class Test{
    
    public static void main(String args[]){
        
        Iterator iter1 = new Iterator(){
          
          private List<Integer> list = List.of(-2, 0, 1, 5, 45, 345);
          private int counter;
          
          @Override
          public boolean hasNext(){
              return counter < list.size();
          }
          
          @Override
          public int next(){
              return list.get(counter++);
          }
          
        }; 
        
        Iterator iter2 = new Iterator(){
          
          private List<Integer> list = List.of(-1, 2, 4, 67, 105);
          private int counter;
          
          @Override
          public boolean hasNext(){
              return counter < list.size();
          }
          
          @Override
          public int next(){
              return list.get(counter++);
          }          
          
        };         
        
        SuperIterator it = new SuperIterator(List.of(iter1, iter2));
        
        while(it.hasNext())
            System.out.println(it.next());
        
        
    }
}


 
